ROOT_INC = $(shell root-config --cflags)
ROOT_LIB = $(shell root-config --libs)

all: corsika2root.cpp
	g++ corsika2root.cpp -o corsika2root $(ROOT_INC) $(ROOT_LIB)
