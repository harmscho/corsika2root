# corsika2root

Convert Corsika output into root TTree. Needs a working `root` installation. To make the convertor, just type `make`. It comes with a python interface (tricep: The Root Interface for Corsika Events and Particles). 

## Changes (21/01/2022)

1) Added to the event tree the random number generators and how often they were
called N. cal1 = N mod 1e6, cal2 = N/1e6.
2) Added thinning weights to the particles when available.   
3) Added python interface `tricep` to the root output file using the `uproot` library. Can be installed with a `pip install -e .`

## Changes (24/03/2016)

1) added a tree "event" for event infomation containing the following fields:

ishow (interger) shower nummer -> same as in "particles" trees
prim_id (interger) corsika particle id of primary particle
prim_energy  (float) energy of primary particle in GeV
theta (float) zenith angle of primary particle in degree
phi (float) phi angle (in corsika system) of primary particle in degree
ground_level (float) altitude a.s.l. for the recorded particles in meters
X0 (float)  depth of first interaction [g/cm^2]
Xmax (float)  shower maximum from longitudinal fit [g/cm^2]

2) the id in the particle tree now is the corsika particle id (previous version you had to divide it by 1000 to get the corsika particle id)
