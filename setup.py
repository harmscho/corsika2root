from setuptools import find_packages, setup

setup(
    name='tricep',
    packages=find_packages('tricep'),
    version='0.0.0',
    description='The Rootfile Interface for Corsika Events and Particles',
    author='Harm Schoorlemmer',
    license='MIT')
