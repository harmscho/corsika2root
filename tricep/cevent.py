import uproot as ur
import numpy as np
import os

class CLongEvent:
    '''
    Longitudal information
    '''
    def __init__(self,filename):
        if os.path.exists(filename):
            fs = open(filename,'r')
            lines = fs.readlines()
            l1 = []
            nb = 0
            for il,l in enumerate(lines[2:]):
                if 'LONGITUDINAL' in l:
                    nb = il+2
                    break
                else:
                    l1.append(l)
            self.X,self.gammas,self.positrons,self.electrons,self.mu_plus,self.mu_min,self.hardons,self.charged,self.nuclei,self.cherenkov = np.loadtxt(l1,unpack=True)
            l2 = []
            for il,l in enumerate(lines[nb+2:-6]):
                l2.append(l)
            self.XE,self.gammasE,self.EM_IONIZ,self.EM_CUT,self.MU_IONEZ,self.MU_CUT,self.HADR_IONIZ,self.HADR_CUT,self.NEUTRINO,self.SUM = np.loadtxt(l2,unpack=True)

            self.P1,self.P2,self.P3,self.P4,self.P5,self.P6 = np.loadtxt(lines[-4].split()[2:],unpack=True)
    def hillas(self,T):
        P1 = self.P1
        P2 = self.P2
        P3 = self.P3
        P4 = self.P4
        P5 = self.P5
        P6 = self.P6
        N = P1*((T-P2)/(P3-P2))**((P3-P2)/(P4+P5*T+P6*T**2)) * np.exp((P3-T)/(P4+P5*T+P6*T**2))
        return N

class CEvent:
    ''' Event structure'''
    def __init__(self):
        self.x = None
        self.y = None
        self.px = None
        self.py = None
        self.pz = None
        self.t = None
        self.id = None
        self.thinw = None
        self.prim_id = None
        self.prim_energy = None
        self.prim_phi= None
        self.prim_theta= None
        self.X0 = None
        self.Xmax = None

# class CLongEvent:
#     '''
#     Longitudal information
#     '''
#     def __init__(self,filename):
#         if os.exist(lfile):
#             fs = open(lfile,'r')
#             lines = fs.readlines()
#             l1 = []
#             for il,l in enumarate(lines[2:]):
#                 if 'LONGITUDINAL' in l:
#                     break
#                 else
#                 l1.append(l)
#             print(l1)

        # else:
            # print('no such long file')


class CRootFileIterator:
        def __init__(self,CRootFile):
            self._CRootFile = CRootFile
            self.ipb = 0
            self.ipe = 0
            self.ie = 0
            self._CRootFile.current_ishow = self._CRootFile.eishow[0]

        def get_event(self):
            # print(self.ipb,self.ipe)
            '''sets indices correct for event '''
            self._CRootFile.event.x = self._CRootFile.x[self.ipb:self.ipe]
            self._CRootFile.event.y = self._CRootFile.y[self.ipb:self.ipe]
            self._CRootFile.event.px = self._CRootFile.px[self.ipb:self.ipe]
            self._CRootFile.event.py = self._CRootFile.py[self.ipb:self.ipe]
            self._CRootFile.event.pz = self._CRootFile.pz[self.ipb:self.ipe]
            self._CRootFile.event.t = self._CRootFile.t[self.ipb:self.ipe]
            self._CRootFile.event.id = self._CRootFile.id[self.ipb:self.ipe]
            if "thinw" in self._CRootFile.p.keys():
                self._CRootFile.event.thinw = self._CRootFile.thinw[self.ipb:self.ipe]
            self._CRootFile.event.prim_id = self._CRootFile.prim_id[self.ie]
            self._CRootFile.event.prim_energy = self._CRootFile.prim_energy[self.ie]
            self._CRootFile.event.prim_phi= self._CRootFile.prim_phi[self.ie]
            self._CRootFile.event.prim_theta=self._CRootFile.prim_theta[self.ie]
            self._CRootFile.event.X0 = self._CRootFile.X0[self.ie]
            self._CRootFile.event.Xmax =self._CRootFile.Xmax[self.ie]
            return self._CRootFile.event

        def __next__(self):
            # not first shower, need to update
            if not(self.ipb == 0 and self.ipe == 0):
                self.ie += 1
                self.ipb = self.ipe
                if self.ie >= self._CRootFile.Nevt:
                    raise StopIteration
                self._CRootFile.current_ishow = self._CRootFile.eishow[self.ie]
                if self._CRootFile.pishow[self.ipb] != self._CRootFile.current_ishow:
                    print("Error, event and particles out of sync",self._CRootFile.pishow[self.ipb],self._CRootFile.current_ishow)
                    raise StopIteration
            #increase the particle counter till the end of the event
            while self._CRootFile.pishow[self.ipe] == self._CRootFile.current_ishow:
                     self.ipe += 1
                     if self.ipe >= self._CRootFile.Npart:
                         break
            return self.get_event()

class CRootFile:
    ''' Holds a corsika root file '''
    def __init__(self,filename):
        p = ur.open(filename + ':particles')
        e = ur.open(filename + ':event')
        self.p = p
        self.e = e
        self.eishow = e['ishow'].array(library='np')
        self.pishow = p['ishow'].array(library='np')
        self.current_ishow = self.eishow[0]
        self.Nevt = len(self.eishow)
        self.Npart = len(self.pishow)
        self.x = p["x"].array(library='np')
        self.y = p["y"].array(library='np')
        self.px = p["px"].array(library='np')
        self.py = p["py"].array(library='np')
        self.pz = p["pz"].array(library='np')
        self.t = p["t"].array(library='np')
        self.id = p["id"].array(library='np')
        if "thinw" in p.keys():
            self.thinw = p["thinw"].array(library='np')
        self.prim_id = e["prim_id"].array(library='np')
        self.prim_energy = e["prim_energy"].array(library='np')
        self.prim_phi= e["phi"].array(library='np')
        self.prim_theta= e["theta"].array(library='np')
        self.X0 = e["X0"].array(library='np')
        self.Xmax = e["Xmax"].array(library='np')
        self.event = CEvent()
    def __iter__(self):
       ''' Returns the Iterator object '''
       return CRootFileIterator(self)
